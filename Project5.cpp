﻿#include <iostream>
#include <time.h>

using namespace std;

int main()
{
    setlocale(LC_ALL, "RU");

    const int n = 5;
    int sum = 0;
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int index;

    index = buf.tm_mday % n;

    
    int array[n][n];

    cout << "Массив: " << "\n";

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            array[i][j] = i + j;

            if (i == index)
            {
                sum += array[i][j];
            }

            cout << array[i][j];
        }

        cout << '\n';
    }

    cout << "Сумма элементов массива: " << sum;
}